from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
  return render_template('index.html.j2')

@app.route("/report")
def report():
  return render_template('report.html.j2')

app.run(debug=True, port=8000)
